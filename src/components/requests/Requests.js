import React from 'react'
import {StyleSheet, View} from 'react-native'
import Projects from 'src/store/projects'
import HomeHeader from 'src/components/home/HomeHeader'
import OutgoingRequests from 'src/components/requests/outgoing/OutgoingRequests'
import Auth from 'src/store/auth'
import IncomingRequests from 'src/components/requests/incoming/IncomingRequests'


class Requests extends React.Component {
  state = {}

  render() {
    return (
      <View style={{flex: 1, justifyContent: 'flex-start'}}>
        <HomeHeader navigation={this.props.navigation}/>
        {this.props.isAdviser ? <IncomingRequests/> : <OutgoingRequests/>}
      </View>
    )
  }
}

export default Projects.providers.projects(Auth.providers.auth(Requests))

const styles = StyleSheet.create({
  scene: {
    flex: 1,
  },
})