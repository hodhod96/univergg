import React from 'react'
import {Image, StyleSheet, View} from 'react-native'
import {SPLASH_LOGO} from 'src/assets/styles/icons'
import {SCREEN_WIDTH} from 'src/assets/styles/style'
import Label from 'src/components/common/Label'
import Spinner from 'react-native-spinkit'
import {COLOR_BLUE_DEFAULT, COLOR_WHITE} from 'src/assets/styles/colors'
import NavigationService from 'src/utils/navigationService'
import {messages} from 'src/utils/messages'
import {AsyncStorageGetItem} from '../../utils/asyncStorage'
import Auth from '../../store/auth'
import Projects from '../../store/projects'

class Launch extends React.Component<Props, State> {

  constructor(props) {
    super(props)

    this.state = {
      loading: false,
    }
  }

  componentDidMount() {
    this.setState({loading: true}, () => {
      AsyncStorageGetItem('jwtToken')
        .then((token) => {
          if (token == null || token === '') {
            setTimeout(() => NavigationService.reset(['AuthNavigator']), 2000)
          }
          else {
            console.log('we are logged in')
            this.props.setToken(token)
            this.props.getProfile()
              .then((res) => {
                NavigationService.reset(['MainTabNavigator'])
                console.log('get prof', res)
              })
              .catch((res) => {
                this.props.logout()
                NavigationService.reset(['AuthNavigator'])
                console.log('er', res)
              })
          }
        })
        .catch(() => {
            this.props.logout()
            NavigationService.reset(['AuthNavigator'])
          },
        )
    })
  }

  render() {
    return (
      <View style={style.launchContainer}>
        <Image source={SPLASH_LOGO} style={style.splashImageStyle}/>
        <Label text={messages.APP_DESCRIPTION}/>
        <Spinner style={{position: 'absolute', bottom: 60}} isVisible={this.state.loading} color={COLOR_BLUE_DEFAULT}
                 type={'Circle'}/>
      </View>
    )
  }
}

export default Projects.providers.projects(Auth.providers.auth(Launch))

const style = StyleSheet.create({
  launchContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: COLOR_WHITE,
  },
  splashImageStyle: {
    width: 0.82 * SCREEN_WIDTH,
    resizeMode: 'contain',
    marginBottom: 20,
  },

})