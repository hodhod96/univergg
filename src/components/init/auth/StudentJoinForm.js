import React from 'react'
import {Picker, StyleSheet, View} from 'react-native'
import Label from 'src/components/common/Label'
import {COLOR_DEFAULT_ORANGE, COLOR_MEDIUM_BLUE} from 'src/assets/styles/colors'
import {messages} from 'src/utils/messages'
import {SCREEN_HEIGHT, SCREEN_WIDTH} from 'src/assets/styles/style'
import CustomInput from 'src/components/common/CustomInput'
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view'
import CustomButton from 'src/components/common/Buttons/CustomButton'
import AgreementCheckBox from 'src/components/common/AgreementCheckBox'
import Auth from '../../../store/auth'
import Projects from 'src/store/projects'
import {toStudentLevel} from 'src/utils/farsiUtils'
import NavigationService from 'src/utils/navigationService'

class StudentJoinForm extends React.Component<Props, State> {

  constructor(props) {
    super(props)
    this.state = {
      username: '',
      password: '',
      confirmPass: '',
      errorMessage: ' ',
      checked: false,
      name: '',
      gender: 1,
      age: null,
      phoneNumber: '',
      city: '',
      country: '',
      level: 1,
      email: '',
    }

    this.onPasswordChange = this.onPasswordChange.bind(this)
    this.onUsernameChange = this.onUsernameChange.bind(this)
    this.onConfirmPasswordChange = this.onConfirmPasswordChange.bind(this)
    this.onCheckBoxChange = this.onCheckBoxChange.bind(this)
    this.onNameChange = this.onNameChange.bind(this)
    this.onAgeChange = this.onAgeChange.bind(this)
    this.onPhoneNumberChange = this.onPhoneNumberChange.bind(this)
    this.onCityChange = this.onCityChange.bind(this)
    this.onCountryChange = this.onCountryChange.bind(this)
    this.onEmailChange = this.onEmailChange.bind(this)
    this.onJoin = this.onJoin.bind(this)
  }

  onUsernameChange(value) {
    this.setState({username: value})
  }

  onPasswordChange(value) {
    this.setState({password: value})
  }

  onConfirmPasswordChange(value) {
    this.setState({confirmPass: value})
  }

  onNameChange(value) {
    this.setState({name: value})
  }

  onAgeChange(value) {
    this.setState({age: value})
  }

  onPhoneNumberChange(value) {
    this.setState({phoneNumber: value})
  }

  onCityChange(value) {
    this.setState({city: value})
  }

  onCountryChange(value) {
    this.setState({country: value})
  }

  onEmailChange(value) {
    this.setState({email: value})
  }

  onJoin() {
    if (this.state.username.length === 0 || this.state.password.length === 0) {
      this.setState({errorMessage: 'نام کاربری یا رمز عبور نمی‌توانند خالی باشند'})
    } else if (this.state.password !== this.state.confirmPass) {
      this.setState({errorMessage: 'تکرار رمز مطابقت ندارد'})
    } else if (this.state.checked === false) {
      this.setState({errorMessage: 'برای عضویت در سامانه باید با قوانین موافقت کرده باشید'})
    } else {
      this.props.studentJoin(this.state.username, this.state.password, this.state.name, this.state.email, this.state.gender, this.state.age, this.state.phoneNumber, this.state.country, this.state.city, this.state.level)
        .then((response) => {
          this.props.login(this.state.username, this.state.password)
            .then((response) => {
              this.props.getProfile()
                .catch(() => {
                })
              NavigationService.reset(['MainTabNavigator'])
              console.log('on login', response)
            })
            .catch((error) => {
              console.log(error)
            })
          console.log('res', response)
        })
        .catch((error) => {
          this.setState({errorMessage: 'امکان ثبت‌نام با این مشخصات نیست.'})
          console.log('join error', error)
        })
    }
  }

  onCheckBoxChange() {
    this.setState((prevState) => ({
      checked: !prevState.checked,
    }))
  }

  render() {
    return (
      <View style={style.login}>
        <KeyboardAwareScrollView
          resetScrollToCoords={{x: 0, y: 0}}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={style.loginForm}
        >
          <CustomInput onFocus={() => this.setState({errorMessage: ' '})}
                       label={messages.USERNAME}
                       onChangeText={this.onUsernameChange}/>

          <CustomInput onFocus={() => this.setState({errorMessage: ' '})}
                       onChangeText={this.onPasswordChange}
                       customInputContainerStyle={{marginTop: 25}}
                       secureTextEntry
                       label={messages.PASSWORD}/>

          <CustomInput onFocus={() => this.setState({errorMessage: ' '})}
                       onChangeText={this.onConfirmPasswordChange}
                       customInputContainerStyle={{marginTop: 25}}
                       secureTextEntry
                       label={messages.CONFIRM_PASS}/>
          <CustomInput onFocus={() => this.setState({errorMessage: ' '})}
                       onChangeText={this.onEmailChange}
                       customInputContainerStyle={{marginTop: 25}}
                       label={messages.EMAIL}/>
          <CustomInput onFocus={() => this.setState({errorMessage: ' '})}
                       onChangeText={this.onNameChange}
                       customInputContainerStyle={{marginTop: 25}}
                       label={messages.NAME}/>

          <View style={{
            width: .85 * SCREEN_WIDTH,
            borderBottomColor: COLOR_MEDIUM_BLUE,
            borderBottomWidth: 2,
            marginTop: 25,
            flexDirection: 'row',
            justifyContent: 'flex-end',
            paddingRight: 0.027 * SCREEN_WIDTH,
          }}>
            <Picker
              mode={'dropdown'}
              selectedValue={this.state.gender}
              style={{
                width: 100,
              }}
              onValueChange={(itemValue, itemIndex) => {
                this.setState({gender: itemValue})
              }}
              itemStyle={{fontFamily: 'IRANSansMobile', fontSize: 25}}>
              <Picker.Item label={messages.WOMAN} value={2}/>
              <Picker.Item label={messages.MAN} value={1}/>
            </Picker>
            <Label text={messages.GENDER} textStyle={{
              color: COLOR_MEDIUM_BLUE,
              fontSize: 18,
              fontFamily: 'IRANSansMobile',
              textAlign: 'right',
            }}/>
          </View>

          <CustomInput onFocus={() => this.setState({errorMessage: ' '})}
                       onChangeText={this.onAgeChange}
                       customInputContainerStyle={{marginTop: 25}}
                       label={messages.AGE_T}/>

          <CustomInput onFocus={() => this.setState({errorMessage: ' '})}
                       onChangeText={this.onPhoneNumberChange}
                       customInputContainerStyle={{marginTop: 25}}
                       label={messages.PHONE_NUMBER_T}/>

          <CustomInput onFocus={() => this.setState({errorMessage: ' '})}
                       onChangeText={this.onCountryChange}
                       customInputContainerStyle={{marginTop: 25}}
                       label={messages.COUNTRY_T}/>

          <CustomInput onFocus={() => this.setState({errorMessage: ' '})}
                       onChangeText={this.onCityChange}
                       customInputContainerStyle={{marginTop: 25}}
                       label={messages.CITY_T}/>

          <View style={{
            width: .85 * SCREEN_WIDTH,
            borderBottomColor: COLOR_MEDIUM_BLUE,
            borderBottomWidth: 2,
            marginTop: 25,
            flexDirection: 'row',
            justifyContent: 'flex-end',
            paddingRight: 0.027 * SCREEN_WIDTH,
          }}>
            <Picker
              mode={'dropdown'}
              selectedValue={this.state.level}
              style={{
                width: 150,
              }}
              onValueChange={(itemValue) => {
                this.setState({level: itemValue})
              }}
              itemStyle={{fontFamily: 'IRANSansMobile', fontSize: 25}}>
              {[1, 2, 3].map((item) => (
                <Picker.Item key={item} label={toStudentLevel(item)} value={item}/>
              ))}
            </Picker>
            <Label text={messages.STUDENT_LEVEL} textStyle={{
              color: COLOR_MEDIUM_BLUE,
              fontSize: 18,
              fontFamily: 'IRANSansMobile',
              textAlign: 'right',
            }}/>
          </View>

          <Label style={{marginTop: 10, marginBottom: 0.03 * SCREEN_HEIGHT}} text={this.state.errorMessage}
                 textStyle={{color: COLOR_DEFAULT_ORANGE, fontSize: 16}}/>

          <AgreementCheckBox style={{marginBottom: 0.06 * SCREEN_HEIGHT}} checked={this.state.checked}
                             onValueChange={this.onCheckBoxChange}/>

          <CustomButton label={messages.SIGN_UP} onPress={this.onJoin}/>

        </KeyboardAwareScrollView>
      </View>
    )
  }
}

export default Projects.providers.projects(Auth.providers.auth(StudentJoinForm))

const style = StyleSheet.create({
  login: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 0.05 * SCREEN_HEIGHT,
  },
  loginForm: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 50,
  },
})