import React from 'react'
import {Picker, StyleSheet, View} from 'react-native'
import Label from 'src/components/common/Label'
import {COLOR_DEFAULT_ORANGE, COLOR_MEDIUM_BLUE, COLOR_WHITE} from 'src/assets/styles/colors'
import {messages} from 'src/utils/messages'
import {SCREEN_HEIGHT, SCREEN_WIDTH} from 'src/assets/styles/style'
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view'
import CustomButton from 'src/components/common/Buttons/CustomButton'
import {toEducationLevel} from 'src/utils/farsiUtils'
import Auth from 'src/store/auth'
import CommonHeader from 'src/components/common/CommonHeader'
import EditInput from 'src/components/common/EditInput'

class AdviserEditProfile extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      errorMessage: ' ',
      user: {
        name: this.props.student.name,
        gender: this.props.student.gender,
        age: this.props.student.age,
        phoneNumber: this.props.student.phoneNumber,
        city: this.props.student.city,
        country: this.props.student.country,
        education: this.props.student.education,
        record: this.props.student.record,
      },
    }

    this.onNameChange = this.onNameChange.bind(this)
    this.onAgeChange = this.onAgeChange.bind(this)
    this.onPhoneNumberChange = this.onPhoneNumberChange.bind(this)
    this.onCityChange = this.onCityChange.bind(this)
    this.onCountryChange = this.onCountryChange.bind(this)
    this.onRecordChange = this.onRecordChange.bind(this)
    this.onEdit = this.onEdit.bind(this)
  }

  onNameChange(value) {
    this.setState({user: {...this.state.user, name: value}})
  }

  onAgeChange(value) {
    this.setState({user: {...this.state.user, age: value}})
  }

  onPhoneNumberChange(value) {
    this.setState({user: {...this.state.user, phoneNumber: value}})
  }

  onCityChange(value) {
    this.setState({user: {...this.state.user, city: value}})
  }

  onCountryChange(value) {
    this.setState({user: {...this.state.user, country: value}})
  }

  onRecordChange(value) {
    this.setState({user: {...this.state.user, record: value}})
  }

  onEdit() {
    this.props.adviserEdit(this.state.user)
      .then((response) => {
        this.props.getProfile()
          .then((res) => {
            console.log(res)
            this.props.navigation.goBack()
          })
          .catch(() => {
            this.props.navigation.goBack()
          })
        console.log('res', response)
      })
      .catch((error) => {
        this.setState({errorMessage: 'امکان ثبت تغییرات نیست. دقتی کنید:\n' + messages.DETAILED_ERROR})
        console.log('join error', error)
      })

  }

  render() {
    return (
      <View style={style.login}>
        <CommonHeader hasBack={true} title={messages.EDIT} onPress={() => this.props.navigation.goBack()}/>
        <KeyboardAwareScrollView
          resetScrollToCoords={{x: 0, y: 0}}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={style.loginForm}
        >
          <EditInput onFocus={() => this.setState({errorMessage: ' '})}
                     onChangeText={this.onNameChange}
                     defaultValue={this.state.user.name}
                     label={messages.NAME}
                     customInputContainerStyle={{marginTop: 25}}
          />

          <View style={{
            width: .85 * SCREEN_WIDTH,
            borderBottomColor: COLOR_MEDIUM_BLUE,
            borderBottomWidth: 2,
            marginTop: 25,
            flexDirection: 'row',
            justifyContent: 'flex-end',
            paddingRight: 0.027 * SCREEN_WIDTH,
          }}>
            <Picker
              mode={'dropdown'}
              selectedValue={parseInt(this.state.user.gender)}
              style={{
                width: 100,
              }}
              onValueChange={(itemValue, itemIndex) => {
                this.setState({user: {...this.state.user, gender: itemValue}})
              }}
              itemStyle={{fontFamily: 'IRANSansMobile', fontSize: 25}}>
              <Picker.Item label={messages.WOMAN} value={2}/>
              <Picker.Item label={messages.MAN} value={1}/>
            </Picker>
            <Label text={messages.GENDER} textStyle={{
              color: COLOR_MEDIUM_BLUE,
              fontSize: 18,
              fontFamily: 'IRANSansMobile',
              textAlign: 'right',
            }}/>
          </View>

          <EditInput onFocus={() => this.setState({errorMessage: ' '})}
                     onChangeText={this.onAgeChange}
                     defaultValue={this.state.user.age.toString()}
                     customInputContainerStyle={{marginTop: 25}}
                     label={messages.AGE_T}/>

          <EditInput onFocus={() => this.setState({errorMessage: ' '})}
                     onChangeText={this.onPhoneNumberChange}
                     defaultValue={this.state.user.phoneNumber}
                     customInputContainerStyle={{marginTop: 25}}
                     label={messages.PHONE_NUMBER_T}/>

          <EditInput onFocus={() => this.setState({errorMessage: ' '})}
                     onChangeText={this.onCountryChange}
                     defaultValue={this.state.user.country}
                     customInputContainerStyle={{marginTop: 25}}
                     label={messages.COUNTRY_T}/>

          <EditInput onFocus={() => this.setState({errorMessage: ' '})}
                     onChangeText={this.onCityChange}
                     defaultValue={this.state.user.city}
                     customInputContainerStyle={{marginTop: 25}}
                     label={messages.CITY_T}/>

          <View style={{
            width: .85 * SCREEN_WIDTH,
            borderBottomColor: COLOR_MEDIUM_BLUE,
            borderBottomWidth: 2,
            marginTop: 25,
            flexDirection: 'row',
            justifyContent: 'flex-end',
            paddingRight: 0.027 * SCREEN_WIDTH,
          }}>
            <Picker
              mode={'dropdown'}
              selectedValue={parseInt(this.state.user.education)}
              style={{
                width: 150,
              }}
              onValueChange={(itemValue) => {
                this.setState({user: {...this.state.user, education: itemValue}})
              }}
              itemStyle={{fontFamily: 'IRANSansMobile', fontSize: 25}}>
              {[1, 2, 3, 4].map((item) => (
                <Picker.Item key={item} label={toEducationLevel(item)} value={item}/>
              ))}
            </Picker>
            <Label text={messages.EDUCATION} textStyle={{
              color: COLOR_MEDIUM_BLUE,
              fontSize: 18,
              fontFamily: 'IRANSansMobile',
              textAlign: 'right',
            }}/>
          </View>

          <EditInput onFocus={() => this.setState({errorMessage: ' '})}
                     onChangeText={this.onRecordChange}
                     defaultValue={this.state.user.record}
                     customInputContainerStyle={{marginTop: 25}}
                     label={messages.RECORD}/>

          <Label style={{marginTop: 10, marginBottom: 0.03 * SCREEN_HEIGHT}} text={this.state.errorMessage}
                 textStyle={{color: COLOR_DEFAULT_ORANGE, fontSize: 16}}/>

          <CustomButton label={messages.SUBMIT} onPress={this.onEdit}/>

        </KeyboardAwareScrollView>
      </View>
    )
  }
}

export default Auth.providers.auth(AdviserEditProfile)

const style = StyleSheet.create({
  login: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: COLOR_WHITE,
  },
  loginForm: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 50,
  },
})