import React from 'react'
import {ScrollView, StyleSheet, View} from 'react-native'
import {SCREEN_HEIGHT, SCREEN_WIDTH} from 'src/assets/styles/style'
import {COLOR_DEFAULT_GRAY, COLOR_WHITE,} from 'src/assets/styles/colors'
import {messages} from 'src/utils/messages'
import ProjectInfoRow from 'src/components/home/project/ProjectInfoRow'
import {toFarsiGender, toStudentLevel} from 'src/utils/farsiUtils'
import CustomButton from 'src/components/common/Buttons/CustomButton'


class StudentProfile extends React.Component<Props, State> {

  render() {
    const {student, editable, onClick} = this.props
    return (
      <ScrollView contentContainerStyle={{
        paddingTop: 20, alignItems: 'flex-end', paddingBottom: 20, backgroundColor: COLOR_WHITE, paddingHorizontal: 20,
      }}>
        {editable && <View style={{alignSelf: 'flex-start'}}>
          <CustomButton onPress={onClick} label={messages.EDIT}/>
        </View>}
        <View style={style.projectInfoContainer}>
          <ProjectInfoRow title={messages.USERNAME + ' '} description={[student.user.username]}/>
          <ProjectInfoRow title={messages.EMAIL + ' '} description={[student.user.email]}/>

          <ProjectInfoRow title={messages.NAME + ' '} description={[student.name]}/>

          <ProjectInfoRow title={messages.GENDER_T + ' '} description={[toFarsiGender(student.gender)]}/>
          <ProjectInfoRow title={messages.AGE_T + ' '} description={[student.age]}/>
          <ProjectInfoRow title={messages.PHONE_NUMBER_T + ' '} description={[student.phoneNumber]}/>
          <ProjectInfoRow title={messages.COUNTRY_T + ' '} description={[student.country]}/>
          <ProjectInfoRow title={messages.CITY_T + ' '} description={[student.city]}/>

          <ProjectInfoRow title={messages.STUDENT_LEVEL + ' '} description={[toStudentLevel(student.grade)]}/>


        </View>
      </ScrollView>
    )
  }
}

export default StudentProfile

const style = StyleSheet.create({
  profileContainer: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  profileHeaderStyle: {
    width: SCREEN_WIDTH,
    paddingHorizontal: 20,
    paddingVertical: 0.03 * SCREEN_HEIGHT,
    backgroundColor: COLOR_WHITE,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    borderBottomColor: COLOR_DEFAULT_GRAY,
    borderBottomWidth: 0.8,
  },
  profileAvatar: {width: 60, height: 60, borderRadius: 30, marginLeft: 20},
  logoutButton: {position: 'absolute', top: 15, left: 10, flexDirection: 'row', alignItems: 'center'},
})