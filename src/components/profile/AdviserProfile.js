import React from 'react'
import {ScrollView, StyleSheet, View} from 'react-native'
import {SCREEN_HEIGHT, SCREEN_WIDTH} from 'src/assets/styles/style'
import {COLOR_BLUE_DEFAULT, COLOR_DEFAULT_GRAY, COLOR_WHITE,} from 'src/assets/styles/colors'
import {messages} from 'src/utils/messages'
import ProjectInfoRow from 'src/components/home/project/ProjectInfoRow'
import {toEducationLevel, toFarsiGender} from 'src/utils/farsiUtils'
import CustomButton from 'src/components/common/Buttons/CustomButton'
import Label from 'src/components/common/Label'


class AdviserProfile extends React.Component<Props, State> {

  render() {
    const {adviser, editable, onClick} = this.props
    return (
      <ScrollView contentContainerStyle={{
        paddingTop: 20, alignItems: 'flex-end', paddingBottom: 20, backgroundColor: COLOR_WHITE, paddingHorizontal: 20,
      }}>
        <View style={{alignSelf: 'flex-start'}}>
          <CustomButton onPress={onClick} label={editable ? messages.EDIT : messages.SEND_REQUEST}/>
        </View>
        <View style={style.projectInfoContainer}>
          <ProjectInfoRow title={messages.USERNAME + ' '} description={[adviser.user.username]}/>
          <ProjectInfoRow title={messages.EMAIL + ' '} description={[adviser.user.email]}/>

          <ProjectInfoRow title={messages.NAME + ' '} description={[adviser.name]}/>

          <ProjectInfoRow title={messages.GENDER_T + ' '} description={[toFarsiGender(adviser.gender)]}/>
          <ProjectInfoRow title={messages.AGE_T + ' '} description={[adviser.age]}/>
          <ProjectInfoRow title={messages.PHONE_NUMBER_T + ' '} description={[adviser.phoneNumber]}/>
          <ProjectInfoRow title={messages.COUNTRY_T + ' '} description={[adviser.country]}/>
          <ProjectInfoRow title={messages.CITY_T + ' '} description={[adviser.city]}/>
          <ProjectInfoRow title={messages.STUDENT_LEVEL + ' '} description={[toEducationLevel(adviser.education)]}/>
          <ProjectInfoRow title={messages.RECORD + ' '}/>
          <Label style={{alignSelf: 'flex-start'}} textStyle={{
            color: COLOR_BLUE_DEFAULT,
            textAlign: 'auto',
            fontFamily: 'IRANSansMobile_Bold'
          }} text={adviser.record}/>
        </View>
      </ScrollView>
    )
  }
}

export default AdviserProfile

const style = StyleSheet.create({
  profileContainer: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  profileHeaderStyle: {
    width: SCREEN_WIDTH,
    paddingHorizontal: 20,
    paddingVertical: 0.03 * SCREEN_HEIGHT,
    backgroundColor: COLOR_WHITE,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    borderBottomColor: COLOR_DEFAULT_GRAY,
    borderBottomWidth: 0.8,
  },
  profileAvatar: {width: 60, height: 60, borderRadius: 30, marginLeft: 20},
  logoutButton: {position: 'absolute', top: 15, left: 10, flexDirection: 'row', alignItems: 'center'},
})