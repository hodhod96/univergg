import React from 'react'
import {Image, StyleSheet, TouchableOpacity, View} from 'react-native'
import {SCREEN_HEIGHT, SCREEN_WIDTH} from 'src/assets/styles/style'
import {COLOR_BLACK, COLOR_BLUE_DEFAULT, COLOR_DEFAULT_GRAY, COLOR_WHITE,} from 'src/assets/styles/colors'
import {messages} from 'src/utils/messages'
import VerifyPopUp from 'src/components/common/popUps/VerifyPopUp'
import Label from 'src/components/common/Label'
import {DEFAULT_PROFILE_PIC, ICON_LOG_OUT} from 'src/assets/styles/icons'
import NavigationService from 'src/utils/navigationService'
import Auth from '../../store/auth'
import StudentProfile from 'src/components/profile/StudentProfile'
import AdviserProfile from 'src/components/profile/AdviserProfile'


class Profile extends React.Component<Props, State> {

  state = {
    addAbilityPopUpVisible: false,
    removeAbilityPopUpVisible: false,
    itemToRemove: '',
    logoutPopup: false,
  }

  constructor(props) {
    super(props)

    this.onPressAddAbility = this.onPressAddAbility.bind(this)
    this.onPressRemoveAbility = this.onPressRemoveAbility.bind(this)
    this.onLogout = this.onLogout.bind(this)
  }

  onPressAddAbility = () => {
    this.setState({addAbilityPopUpVisible: true})
  }
  onPressRemoveAbility = (item) => {
    console.log(item)
    this.setState({removeAbilityPopUpVisible: true, itemToRemove: item})
  }

  onLogout = () => {
    this.props.logout()
    NavigationService.reset(['AuthNavigator'])
  }

  render() {
    return (
      <View style={{flex: 1, justifyContent: 'flex-start'}}>
        <View style={style.profileHeaderStyle}>
          <TouchableOpacity
            onPress={() => this.setState({logoutPopup: true})}
            style={style.logoutButton}>
            <Image source={ICON_LOG_OUT} style={{width: 25, height: 25, marginLeft: 7}}
                   tintColor={COLOR_BLUE_DEFAULT}/>
          </TouchableOpacity>

          <View style={{flexDirection: 'column', alignItems: 'flex-end'}}>
            <Label text={this.props.student.user.username} textStyle={{fontSize: 18, color: COLOR_BLACK}}/>
            <Label text={this.props.isAdviser ? 'مشاور' : 'دانشجو'} textStyle={{fontSize: 18, color: COLOR_BLACK}}/>
          </View>

          <Image source={DEFAULT_PROFILE_PIC} style={style.profileAvatar}/>
        </View>

        {this.props.isAdviser ?
          <AdviserProfile onClick={() => this.props.navigation.navigate('AdviserEditProfile')} editable={true}
                          adviser={this.props.student}/> :
          <StudentProfile onClick={() => this.props.navigation.navigate('StudentEditProfile')} editable={true}
                          student={this.props.student}/>}

        <VerifyPopUp visible={this.state.logoutPopup}
                     verifyText={messages.LOGOUT_MESSAGE}
                     onVerify={this.onLogout}
                     onDismiss={() => this.setState({logoutPopup: false})}/>
      </View>
    )
  }
}

export default Auth.providers.auth(Profile)

const style = StyleSheet.create({
  profileContainer: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  profileHeaderStyle: {
    width: SCREEN_WIDTH,
    paddingHorizontal: 20,
    paddingVertical: 0.03 * SCREEN_HEIGHT,
    backgroundColor: COLOR_WHITE,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    borderBottomColor: COLOR_DEFAULT_GRAY,
    borderBottomWidth: 0.8,
  },
  profileAvatar: {width: 60, height: 60, borderRadius: 30, marginLeft: 20},
  logoutButton: {position: 'absolute', top: 15, left: 10, flexDirection: 'row', alignItems: 'center'},
})