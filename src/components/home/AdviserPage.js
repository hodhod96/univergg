import React from 'react'
import {Image, Keyboard, StyleSheet, View} from 'react-native'
import {SCREEN_HEIGHT, SCREEN_WIDTH} from 'src/assets/styles/style'
import {COLOR_BLACK, COLOR_DEFAULT_GRAY, COLOR_WHITE,} from 'src/assets/styles/colors'
import Label from 'src/components/common/Label'
import {DEFAULT_PROFILE_PIC} from 'src/assets/styles/icons'
import StudentProfile from 'src/components/profile/StudentProfile'
import AdviserProfile from 'src/components/profile/AdviserProfile'
import CommonHeader from 'src/components/common/CommonHeader'
import InputMessagePopUp from 'src/components/common/popUps/InputMessagePopUp'
import {messages} from 'src/utils/messages'
import Projects from 'src/store/projects'


class AdviserPage extends React.Component<Props, State> {

  constructor(props) {
    super(props)

    this.state = {
      sendRequestPopup: false,
    }
  }

  render() {
    const adviser = this.props.navigation.getParam('adviser', {})
    return (
      <View style={{flex: 1, justifyContent: 'flex-start'}}>
        <CommonHeader hasBack={true} title={'پروفایل مشاور'} onPress={() => this.props.navigation.goBack()}/>
        <View style={style.profileHeaderStyle}>
          <View style={{flexDirection: 'column', alignItems: 'flex-end'}}>
            <Label text={adviser.user.username} textStyle={{fontSize: 18, color: COLOR_BLACK}}/>
            <Label text={adviser.user.isAdvisor ? 'مشاور' : 'دانشجو'} textStyle={{fontSize: 18, color: COLOR_BLACK}}/>
          </View>

          <Image source={DEFAULT_PROFILE_PIC} style={style.profileAvatar}/>
        </View>

        {adviser.user.isAdvisor ?
          <AdviserProfile onClick={() => this.setState({sendRequestPopup: true})} editable={false} adviser={adviser}/> :
          <StudentProfile editable={false}/>}

        <InputMessagePopUp visible={this.state.sendRequestPopup}
                           title={messages.SEND_REQUEST}
                           text={messages.REQUEST_MESSAGE}
                           onSend={(message) => {
                             this.props.sendRequestToAdviser(adviser.user.id, message)
                               .then((res) => {
                                 console.log('send request response', res)
                                 this.props.getOutgoingRequests()
                                   .then((response) => {
                                     console.log('sent requests', response)
                                   })
                                   .catch((err) => {
                                     console.log('get outgoing projects', err)
                                   })
                               })
                               .catch((error) => {
                                 console.log('send request error', error)
                               })
                           }}
                           onDismiss={() => {
                             Keyboard.dismiss()
                             this.setState({sendRequestPopup: false})
                           }}/>
      </View>
    )
  }
}

export default Projects.providers.projects(AdviserPage)

const style = StyleSheet.create({
  profileContainer: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  profileHeaderStyle: {
    width: SCREEN_WIDTH,
    paddingHorizontal: 20,
    paddingVertical: 0.03 * SCREEN_HEIGHT,
    backgroundColor: COLOR_WHITE,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    borderBottomColor: COLOR_DEFAULT_GRAY,
    borderBottomWidth: 0.8,
  },
  profileAvatar: {width: 60, height: 60, borderRadius: 30, marginLeft: 20},
  logoutButton: {position: 'absolute', top: 15, left: 10, flexDirection: 'row', alignItems: 'center'},
})