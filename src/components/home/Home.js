import React from 'react'
import {ScrollView, StyleSheet, View} from 'react-native'
import HomeHeader from 'src/components/home/HomeHeader'
import Projects from '../../store/projects'
import AdviserItem from 'src/components/home/AdviserItem'
import Auth from 'src/store/auth'
import Label from 'src/components/common/Label'


class Home extends React.Component<Props, State> {

  constructor(props) {
    super(props)

    this.navigateToAdviserProfile = this.navigateToAdviserProfile.bind(this)
  }

  componentDidMount() {
    if (!this.props.isAdviser) {
      this.props.getOutgoingRequests()
        .then((res) => {
          console.log('out req', res)
        })
        .catch((err) => {
          console.log('out err', err)
        })
      this.props.getAdvisers()
        .then((resp) => {
          console.log('advisers', resp)
        })
        .catch((err) => {
          console.log('er', err)
        })
    } else {
      this.props.getIncomingRequests()
        .then((res) => {
          console.log('in req', res)
        })
        .catch((err) => {
          console.log('in err', err)
        })
    }

  }

  navigateToAdviserProfile(adviser) {
    console.log('navigation', this.props.navigation)
    this.props.navigation.navigate({routeName: 'AdviserPage', params: {adviser}})
  }


  render() {
    return (
      <View style={{justifyContent: 'flex-start', flex: 1}}>
        <HomeHeader showSearch={!this.props.isAdviser} navigation={this.props.navigation}/>
        {this.props.isAdviser ?
          <View style={{marginTop: 100}}>
            <Label text={'NOTHING TO SEE HERE YET! CHOOSE OTHER TABS.'}/>
          </View> :
          <ScrollView contentContainerStyle={{paddingTop: 20, alignItems: 'center'}}>
            {
              this.props.advisers.map((item, index) =>
                (
                  <AdviserItem onClick={() => this.navigateToAdviserProfile(item)} key={index.toString()} user={item}/>
                ),
              )
            }
          </ScrollView>}
      </View>
    )
  }
}

export default Projects.providers.projects(Auth.providers.auth(Home))

const style = StyleSheet.create({})