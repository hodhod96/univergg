import React from 'react'
import {Image, StyleSheet, TouchableOpacity, View} from 'react-native'
import {SCREEN_HEIGHT, SCREEN_WIDTH} from 'src/assets/styles/style'
import {COLOR_BLACK, COLOR_WHITE,} from 'src/assets/styles/colors'
import Label from 'src/components/common/Label'
import {DEFAULT_PROFILE_PIC} from 'src/assets/styles/icons'


class AdviserItem extends React.Component {

  render() {
    return (
      <TouchableOpacity style={style.containerStyle} onPress={this.props.onClick}>
        <View style={style.profileHeaderStyle}>
          <Label text={this.props.user.user.username} textStyle={{fontSize: 18, color: COLOR_BLACK}}/>

          <Image source={DEFAULT_PROFILE_PIC} style={style.profileAvatar}/>
        </View>

        <View style={{flexDirection: 'column'}}>
          <Label text={this.props.user.record} textStyle={{fontSize: 14, color: COLOR_BLACK, textAlign: 'auto'}}/>
        </View>

      </TouchableOpacity>
    )
  }
}

export default AdviserItem

const style = StyleSheet.create({
  containerStyle: {
    width: SCREEN_WIDTH * 0.92,
    borderRadius: 20,
    backgroundColor: COLOR_WHITE,
    alignSelf: 'center',
    marginBottom: 20,
    paddingBottom: 15,
    paddingHorizontal: 20,
    elevation: 1,
  },
  profileHeaderStyle: {
    paddingHorizontal: 20,
    paddingVertical: 0.03 * SCREEN_HEIGHT,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    flexWrap: 'wrap',
  },
  profileAvatar: {width: 60, height: 60, borderRadius: 30, marginLeft: 20},
  logoutButton: {position: 'absolute', top: 15, left: 10, flexDirection: 'row', alignItems: 'center'},

})