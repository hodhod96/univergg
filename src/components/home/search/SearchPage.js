import React from 'react'
import {ScrollView, StyleSheet, View} from 'react-native'
import CommonHeader from 'src/components/common/CommonHeader'
import {messages} from 'src/utils/messages'
import SearchBar from 'src/components/home/search/SearchBar'
import Projects from 'src/store/projects'
import AdviserItem from 'src/components/home/AdviserItem'

class SearchPage extends React.Component<Props, State> {

  constructor(props) {
    super(props)

    this.onSearch = this.onSearch.bind(this)
  }

  state = {
    advisers: [],
  }

  onSearch(name, filters) {
    if (name != null && name !== '')
      filters['name'] = name
    console.log('on search', filters)
    this.props.searchAdvisers(filters)
      .then((response) => {
        this.setState({advisers: response})
        console.log('search result', response)
      })
      .catch((error) => {
        console.log('er search', error)
      })
  }

  navigateToAdviserProfile(adviser) {
    this.props.navigation.navigate({routeName: 'AdviserPage', params: {adviser}})
  }


  render() {
    let filters = this.props.navigation.getParam('filters', {})
    return (
      <View style={{justifyContent: 'flex-start', flex: 1, alignItems: 'center'}}>
        <CommonHeader title={messages.SEARCH} hasBack={true} onPress={() => this.props.navigation.goBack('Search')}/>
        <SearchBar navigation={this.props.navigation}
                   onSearch={(name) => this.onSearch(name, filters)}/>
        <ScrollView contentContainerStyle={{paddingTop: 20, alignItems: 'center'}}>
          {
            this.state.advisers.map((item, index) =>
              (
                <AdviserItem onClick={() => this.navigateToAdviserProfile(item)} key={index.toString()} user={item}/>
              ),
            )
          }

        </ScrollView>
      </View>
    )
  }
}

export default Projects.providers.projects(SearchPage)

const style = StyleSheet.create({})