import React from 'react'
import {Picker, ScrollView, StyleSheet, TextInput, View} from 'react-native'
import {messages} from 'src/utils/messages'
import CommonHeader from 'src/components/common/CommonHeader'
import Label from 'src/components/common/Label'
import {SCREEN_WIDTH} from 'src/assets/styles/style'
import CustomButton from 'src/components/common/Buttons/CustomButton'
import {COLOR_WHITE} from 'src/assets/styles/colors'
import Auth from 'src/store/auth'


class FilterPage extends React.Component<Props, State> {

  constructor(props) {
    super(props)

    this.onMinAgeChange = this.onMinAgeChange.bind(this)
    this.onMaxAgeChange = this.onMaxAgeChange.bind(this)
    this.onLocationChange = this.onLocationChange.bind(this)
    this.onSubmitFilters = this.onSubmitFilters.bind(this)
  }

  state = {
    selectedGender: 3,
    minAge: null,
    maxAge: null,
    city: null,
    filters: {},
  }

  clean(obj) {
    for (let propName in obj) {
      if (obj[propName] === null || obj[propName] === undefined) {
        delete obj[propName]
      }
    }
  }

  onMinAgeChange(text) {
    this.setState({minAge: text})
  }

  onMaxAgeChange(text) {
    this.setState({maxAge: text})
  }

  onLocationChange(text) {
    this.setState({city: text})
  }

  onSubmitFilters() {
    const filters = {
      minAge: this.state.minAge,
      maxAge: this.state.maxAge,
      gender: this.state.selectedGender,
      city: this.state.city,
    }
    this.clean(filters)
    const {routeName, key} = this.props.navigation.getParam('returnToRoute')
    this.setState({filters: filters}, () => this.props.navigation.navigate({
      routeName,
      key,
      params: {filters: this.state.filters},
    }))
  }


  render() {
    return (
      <View style={{justifyContent: 'flex-start', flex: 1, alignItems: 'center'}}>
        <CommonHeader title={messages.ADD_FILTER} hasBack={true}
                      onPress={() => {
                        const {routeName, key} = this.props.navigation.getParam('returnToRoute')
                        this.props.navigation.navigate({routeName, key, params: {filters: this.state.filters}})
                      }}/>

        <ScrollView showsVerticalScrollIndicator={false}
                    contentContainerStyle={{paddingTop: 20, alignItems: 'center', paddingBottom: 20}}>

          <View style={style.itemContainerStyle}>
            <Label textStyle={{fontSize: 16}} style={{alignSelf: 'flex-end', marginBottom: 5}}
                   text={messages.GENDER}/>
            <Picker
              mode={'dropdown'}
              selectedValue={this.state.selectedGender}
              style={{height: 50, width: 250}}
              onValueChange={(itemValue, itemIndex) => this.setState({selectedGender: itemValue})}
              itemStyle={{fontFamily: 'IRANSansMobile', fontSize: 25}}>
              <Picker.Item label=' هر دو' value={3}/>
              <Picker.Item label=' زن' value={2}/>
              <Picker.Item label=' مرد' value={1}/>
            </Picker>
          </View>

          <View style={style.itemContainerStyle}>
            <Label textStyle={{fontSize: 16}} style={{alignSelf: 'flex-end', marginBottom: 5}}
                   text={messages.VOLUNTEER_AGE}/>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <TextInput keyboardType={'number-pad'} style={[style.itemTextStyle, {width: 40}]}
                         onChangeText={(text) => this.onMaxAgeChange(text)}/>
              <Label text={' تا '}/>
              <TextInput keyboardType={'number-pad'} style={[style.itemTextStyle, {width: 40}]}
                         onChangeText={(text) => this.onMinAgeChange(text)}/>
              <Label text={'از '}/>
            </View>
          </View>

          <View style={style.itemContainerStyle}>
            <Label textStyle={{fontSize: 16}} style={{alignSelf: 'flex-end', marginBottom: 5}}
                   text={messages.CITY}/>
            <TextInput style={style.itemTextStyle} onChangeText={(text) => this.onLocationChange(text)}/>
          </View>


          <CustomButton style={{width: 0.8 * SCREEN_WIDTH, height: 50}}
                        label={messages.ADD_FILTER}
                        labelStyle={{fontSize: 20}}
                        onPress={this.onSubmitFilters}
          />

        </ScrollView>

      </View>
    )
  }
}

export default Auth.providers.auth(FilterPage)

const style = StyleSheet.create({
  itemContainerStyle: {
    paddingHorizontal: 30,
    paddingTop: 30,
    paddingBottom: 40,
    borderRadius: 10,
    width: SCREEN_WIDTH * 0.95,
    flexDirection: 'column',
    alignItems: 'center',
    marginBottom: 20,
    backgroundColor: COLOR_WHITE,
    elevation: 1,
  },
  itemTextStyle: {
    width: 250,
    fontSize: 16,
    borderBottomWidth: 1,
    fontFamily: 'IRANSansMobile',
    textAlign: 'right',
  },
})