import actionTypes from 'src/store/actionTypes'
import {Constants} from '../../constants'
import Request from 'src/utils/request'


const actions = {
  getAdvisers: () => {
    return {
      types: actionTypes.GET_ADVISERS,
      shouldCallAPI: (state) => state.auth.isLoggedIn,
      callAPI: (state) => Request.get(state, Constants.getAdvisersUrl),
    }
  },
  searchAdvisers: (filters) => {
    return {
      types: actionTypes.SEARCH_ADVISERS,
      shouldCallAPI: (state) => state.auth.isLoggedIn,
      callAPI: (state) => Request.get(state, Constants.getAdvisersUrl, filters),
    }
  },
  sendRequestToAdviser: (adviserID, message) => {
    return {
      types: actionTypes.SEND_REQUEST_TO_ADVISER,
      shouldCallAPI: (state) => state.auth.isLoggedIn,
      callAPI: (state) => Request.post(state, Constants.requestUrl + adviserID + '/', {},
        {message},),
    }
  },
  getOutgoingRequests: () => {
    return {
      types: actionTypes.GET_OUTGOING_REQUESTS,
      shouldCallAPI: (state) => state.auth.isLoggedIn,
      callAPI: (state) => Request.get(state, Constants.outgoingRequestsUrl),
    }
  },
  getIncomingRequests: () => {
    return {
      types: actionTypes.GET_INCOMING_REQUESTS,
      shouldCallAPI: (state) => state.auth.isLoggedIn,
      callAPI: (state) => Request.get(state, Constants.incomingRequestsUrl),
    }
  },
  acceptRequest: (requestID) => {
    return {
      types: actionTypes.ACCEPT_STUDENT_REQUEST,
      shouldCallAPI: (state) => state.auth.isLoggedIn,
      callAPI: (state) => Request.patch(state, Constants.requestResponseUrl + requestID + '/', {}, {accepted: true}),
    }
  },
  rejectRequest: (requestID, reason) => {
    return {
      types: actionTypes.REJECT_STUDENT_REQUEST,
      shouldCallAPI: (state) => state.auth.isLoggedIn,
      callAPI: (state) => Request.patch(state, Constants.requestResponseUrl + requestID + '/', {}, {
        accepted: false,
        rejectionReason: reason,
      }),
    }
  },


}

export default actions