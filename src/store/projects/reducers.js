import {combineReducers} from 'redux'
import actionTypes from '../actionTypes'

const advisers = (state = [], action) => {
  switch (action.type) {
    case actionTypes.GET_ADVISERS.SUCCESS:
      return action.response
    case actionTypes.LOGOUT:
      return []
    default:
      return state
  }
}

const outgoingRequests = (state = [], action) => {
  switch (action.type) {
    case actionTypes.GET_OUTGOING_REQUESTS.SUCCESS:
      return action.response
    case actionTypes.LOGOUT:
      return []
    default:
      return state
  }
}

const incomingRequests = (state = [], action) => {
  switch (action.type) {
    case actionTypes.GET_INCOMING_REQUESTS.SUCCESS:
      return action.response
    case actionTypes.LOGOUT:
      return []
    default:
      return state
  }
}


const reducers = combineReducers({
  advisers,

  outgoingRequests,
  incomingRequests,
})

export default reducers