import {connect} from 'react-redux'
import actions from './actions'


const projectsProvider = connect(
  (state) => {
    return {
      advisers: state.projects.advisers,
      outgoingRequests: state.projects.outgoingRequests,
      incomingRequests: state.projects.incomingRequests,

    }
  }, (dispatch) => {
    return {
      getAdvisers: () => dispatch(actions.getAdvisers()),
      searchAdvisers: (filters) => dispatch(actions.searchAdvisers(filters)),
      sendRequestToAdviser: (adviserID, message) => dispatch(actions.sendRequestToAdviser(adviserID, message)),
      getOutgoingRequests: () => dispatch(actions.getOutgoingRequests()),
      getIncomingRequests: () => dispatch(actions.getIncomingRequests()),
      acceptRequest: (requestID) => dispatch(actions.acceptRequest(requestID)),
      rejectRequest: (requestID, reason) => dispatch(actions.rejectRequest(requestID, reason)),
    }
  },
)

const providers = {
  projects: projectsProvider,
}

export default providers