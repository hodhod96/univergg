import actions from 'src/store/projects/actions'
import providers from 'src/store/projects/providers'
import reducers from 'src/store/projects/reducers'

const Projects = {
  actions: actions,
  reducers: reducers,
  providers: providers,
}

export default Projects