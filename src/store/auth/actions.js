import actionTypes from 'src/store/actionTypes'
import {makeActionCreator} from '../../utils/redux'
import {Constants} from '../../constants'
import Request from 'src/utils/request'


const actions = {
  logout: makeActionCreator(actionTypes.LOGOUT),
  setToken: makeActionCreator(actionTypes.SET_TOKEN, 'token'),
  login: ({username, password}) => {
    return {
      types: actionTypes.LOGIN,
      shouldCallAPI: (state) => !state.auth.isLoggedIn,
      callAPI: (state) => Request.post(state, Constants.loginUrl, {},
        {username, password}, false),
    }
  },
  getProfile: () => {
    return {
      types: actionTypes.GET_PROFILE,
      shouldCallAPI: (state) => state.auth.isLoggedIn,
      callAPI: (state) => Request.get(state, Constants.profileUrl),
    }
  },
  studentJoin: (username, password, name, email, gender, age, phoneNumber, country, city, grade) => {
    return {
      types: actionTypes.STUDENT_JOIN,
      shouldCallAPI: (state) => !state.auth.isLoggedIn,
      callAPI: (state) => Request.post(state, Constants.studentJoinUrl, {},
        {user: {username, password, email}, name, gender, age, phoneNumber, country, city, grade}, false),
    }
  },
  adviserJoin: (user) => {
    return {
      types: actionTypes.ADVISER_JOIN,
      shouldCallAPI: (state) => !state.auth.isLoggedIn,
      callAPI: (state) => Request.post(state, Constants.adviserJoinUrl, {},
        user, false),
    }
  },
  studentEdit: (user) => {
    return {
      types: actionTypes.STUDENT_EDIT,
      shouldCallAPI: (state) => state.auth.isLoggedIn,
      callAPI: (state) => Request.patch(state, Constants.studentEditUrl, {},
        user),
    }
  },
  adviserEdit: (user) => {
    return {
      types: actionTypes.ADVISER_EDIT,
      shouldCallAPI: (state) => state.auth.isLoggedIn,
      callAPI: (state) => Request.patch(state, Constants.adviserEditUrl, {},
        user),
    }
  },

}

export default actions