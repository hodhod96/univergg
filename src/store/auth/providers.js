import actions from 'src/store/auth/actions'
import {connect} from 'react-redux'


const authProvider = connect(
  (state) => {
    return {
      student: state.auth.student,
      isAdviser: state.auth.isAdviser,
    }
  }, (dispatch) => {
    return {
      setToken: (token) => dispatch(actions.setToken(token)),
      login: (username, password) => dispatch(actions.login({username, password})),
      studentJoin: (username, password, name, email, gender, age, phoneNumber, country, city, grade) => dispatch(actions.studentJoin(username, password, name, email, gender, age, phoneNumber, country, city, grade)),
      logout: () => dispatch(actions.logout()),
      getProfile: () => dispatch(actions.getProfile()),
      adviserJoin: (user) => dispatch(actions.adviserJoin(user)),
      studentEdit: (user) => dispatch(actions.studentEdit(user)),
      adviserEdit: (user) => dispatch(actions.adviserEdit(user)),
    }
  },
)

const providers = {
  auth: authProvider,
}

export default providers