import actions from 'src/store/auth/actions'
import providers from 'src/store/auth/providers'
import reducers from 'src/store/auth/reducers'

const Auth = {
    actions: actions,
    reducers: reducers,
    providers: providers,
}

export default Auth