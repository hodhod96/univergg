let asyncActionTypeCreator = (actionType) => {
  return {
    REQUEST: actionType + '_REQUEST',
    SUCCESS: actionType + '_SUCCESS',
    FAILURE: actionType + '_FAILURE',
  }
}

const actionTypes = {
  SET_TOKEN: 'SET_TOKEN',
  SET_ID: 'SET_ID',
  SET_IS_VOLUNTEER: 'SET_IS_VOLUNTEER',
  LOGOUT: 'LOGOUT',
  LOGIN: asyncActionTypeCreator('LOGIN'),
  STUDENT_JOIN: asyncActionTypeCreator('STUDENT_JOIN'),
  ADVISER_JOIN: asyncActionTypeCreator('ADVISER_JOIN'),
  STUDENT_EDIT: asyncActionTypeCreator('STUDENT_EDIT'),
  ADVISER_EDIT: asyncActionTypeCreator('ADVISER_EDIT'),
  GET_PROFILE: asyncActionTypeCreator('GET_PROFILE'),
  GET_ADVISERS: asyncActionTypeCreator('GET_ADVISERS'),
  SEARCH_ADVISERS: asyncActionTypeCreator('SEARCH_ADVISERS'),
  SEND_REQUEST_TO_ADVISER: asyncActionTypeCreator('SEND_REQUEST_TO_ADVISER'),
  GET_OUTGOING_REQUESTS: asyncActionTypeCreator('GET_OUTGOING_REQUESTS'),
  GET_INCOMING_REQUESTS: asyncActionTypeCreator('GET_INCOMING_REQUESTS'),
  ACCEPT_STUDENT_REQUEST: asyncActionTypeCreator('ACCEPT_STUDENT_REQUEST'),
  REJECT_STUDENT_REQUEST: asyncActionTypeCreator('REJECT_STUDENT_REQUEST'),
}

export default actionTypes
