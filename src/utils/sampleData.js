import {
  PROJECT_SAMPLE_PIC1, PROJECT_SAMPLE_PIC2, PROJECT_SAMPLE_PIC3, PROJECT_SAMPLE_PIC4, PROJECT_SAMPLE_PIC5,
  PROJECT_SAMPLE_PIC6, PROJECT_SAMPLE_PIC7,
} from 'src/assets/styles/icons'
import {messages} from 'src/utils/messages'

export const project1 = {
  projectPicture: PROJECT_SAMPLE_PIC4,
  projectName: 'غذارسانی حیوانات',
  charityName: 'پناهگاه وفا',
  projectStartDate: '۱۲ مهر ۱۳۹۷',
  projectEndDate: '۱۲ آذر ',
  projectType: messages.NON_CASH,
  projectDescription: 'این پروژه برای غذارسانی به حیوانات ولگرد مشادهده شده در اطراف باغ‌های هشتگرد است که بسیار ضعیف و رنجور شده‌اند. سرمای هوا باعث شده که این حیوانات حتی از قبل نیز ضعیف‌تر شوند.',
  info: {
    age: '۲۰ تا ۳۰ سال',
    location: 'هشتگرد',
    gender: 'زن - مرد',
    abilities: ['دامپزشکی', 'روحیه گروهی'],
  },
}

export const project2 = {
  projectPicture: PROJECT_SAMPLE_PIC5,
  projectName: 'تامین کمک هزینه دانش‌آموزان',
  charityName: 'خیریه حکمت',
  projectStartDate: '۱ مهر ۱۳۹۷',
  projectEndDate: '۱۰ مهر ۱۳۹۷',
  projectType: messages.CASH,
  projectDescription: 'این پروژه به اهداف تامین کمک هزینه تحصیل کودکان روستای فلان ایجاد شده است. سالانه بین ۱۵ تا ۲۰ هزار دانش‌آموز در پایه‌های ابتدایی ترک تحصیل می‌کنند. فقر، ازدواج زودهنگام، نبود امکانات آموزشی و دلایل گسترده دیگر از مسائلی است که کودکان در حاشیه شهرها را از ادامه تحصیل باز می دارد. پیشتر دولت مخارج اين دانش آموزان را تامین و آن ها را مورد حمايت قرار می داد  ولي الان پنج سال است كه دولت حمايت نمی‌كند و اين دانش آموزان دختر بايد سالی حدود ١/٥ميليون براي خرج خود بپردازند.',
  info: {
    neededAmount: '2000000',
    fundedAmount: '500000',
  },
}

export const project3 = {
  projectPicture: PROJECT_SAMPLE_PIC1,
  projectName: 'کمک به دامداری',
  charityName: 'پناهگاه وفا',
  projectStartDate: '۱۲ مهر ۱۳۹۷',
  projectEndDate: '۱۲ آذر ',
  projectType: messages.NON_CASH,
  projectDescription: 'ربات ها می توانند در کشاورزی و دامداری کمک های بسیاری را برای تولید کننده انجام داشته باشند و کار را برایش آسان کنند . مثلا دستگاه های خودکار شیر دوش ، ربات های تمیز کننده اصطبل و یا ربا ت هایی که می توانند به طور شبانه روزی و با دقت تمام وضعیت دام ها را زیر نظر داشته باشند . همگی دستگاه هایی هستند که بشر برای آسان تر کردن کارهای خود آنها را ساخته است.',
  info: {
    age: '۲۰ تا ۳۰ سال',
    location: 'فرحزاد',
    gender: 'مرد',
    abilities: ['دامپزشکی'],
  },
}

export const project4 = {
  projectPicture: PROJECT_SAMPLE_PIC2,
  projectName: 'ایونت خیریه کویری',
  charityName: 'موسسه مهر',
  projectStartDate: '۱۴ مهر ۱۳۹۷',
  projectEndDate: '۱۰ آذر ',
  projectType: messages.NON_CASH,
  projectDescription: '٤و٥و٦ شهريور ماه امسال بازارچه خيريه پيام اميد در سالن تلاش چهارراه پارك وي برگزار مي شود! قراره كه باز همگي دور هم جمع بشيم تا لبخند هامونو به اشتراك بذاريم! قراره همه با هم خوشحال باشيم! قراره كه شادي هامون فقط براي خودمون نباشه!',
  info: {
    age: '۱۰ تا ۹۰ سال',
    location: 'کویر مرنجاب',
    gender: 'زن - مرد',
    abilities: ['روحیه گروهی'],
  },
}

export const project5 = {
  projectPicture: PROJECT_SAMPLE_PIC3,
  projectName: 'کمک به مادران خیاط',
  charityName: 'موسسه رعد',
  projectStartDate: '۱۴ مهر ۱۳۹۷',
  projectEndDate: '۱۰ آذر ',
  projectType: messages.NON_CASH,
  projectDescription: '٤و٥و٦ شهريور ماه امسال بازارچه خيريه پيام اميد در سالن تلاش چهارراه پارك وي برگزار مي شود! قراره كه باز همگي دور هم جمع بشيم تا لبخند هامونو به اشتراك بذاريم! قراره همه با هم خوشحال باشيم! قراره كه شادي هامون فقط براي خودمون نباشه!',
  info: {
    age: '۳۰ تا ۵۰ سال',
    location: 'تهران',
    gender: 'زن',
    abilities: ['خیاطی'],
  },
}

export const project6 = {
  projectPicture: PROJECT_SAMPLE_PIC6,
  projectName: 'تهیه غذا برای بی‌خانمان‌ها',
  charityName: 'موسسه شاپرک',
  projectStartDate: '۱۴ مهر ۱۳۹۷',
  projectEndDate: '۱۰ آذر ',
  projectType: messages.NON_CASH,
  projectDescription: '٤و٥و٦ شهريور ماه امسال بازارچه خيريه پيام اميد در سالن تلاش چهارراه پارك وي برگزار مي شود! قراره كه باز همگي دور هم جمع بشيم تا لبخند هامونو به اشتراك بذاريم! قراره همه با هم خوشحال باشيم! قراره كه شادي هامون فقط براي خودمون نباشه!',
  info: {
    age: '۲۰ تا ۵۰ سال',
    location: 'تهران',
    gender: 'زن - مرد',
    abilities: ['آشپزی'],
  },
}

export const project7 = {
  projectPicture: PROJECT_SAMPLE_PIC7,
  projectName: 'جمع‌آوری لباس برای بی‌خانمان‌ها',
  charityName: 'موسسه شاپرک',
  projectStartDate: '۱۴ مهر ۱۳۹۷',
  projectEndDate: '۱۰ آذر ',
  projectType: messages.NON_CASH,
  projectDescription: '٤و٥و٦ شهريور ماه امسال بازارچه خيريه پيام اميد در سالن تلاش چهارراه پارك وي برگزار مي شود! قراره كه باز همگي دور هم جمع بشيم تا لبخند هامونو به اشتراك بذاريم! قراره همه با هم خوشحال باشيم! قراره كه شادي هامون فقط براي خودمون نباشه!',
  info: {
    age: '۲۰ تا ۵۰ سال',
    location: 'تهران',
    gender: 'زن - مرد',
    abilities: ['روحیه گروهی'],
  },
}

export const request1 = {
  project: project3,
  status: messages.PENDING,
  message: 'خوشحال میشیم با ما همکاری کنید!',
}

export const request2 = {
  project: project4,
  status: messages.PENDING,
  message: 'توانایی‌های شما در راستای پروژه ماست. لطفا درخواست ما را بررسی کنید!',
}

export const request3 = {
  project: project5,
  status: messages.PENDING,
  message: 'با توجه به پروفایل شما، شما فرد مناسبی برای این امر خیر هستید!',
}

export const request4 = {
  project: project6,
  status: messages.PENDING,
  message: 'با توجه به پروفایل شما، شما فرد مناسبی برای این امر خیر هستید!',
}

export const request5 = {
  project: project7,
  status: messages.REJECTED,
  message: 'با توجه به پروفایل شما، شما فرد مناسبی برای این امر خیر هستید!',
  reason: 'متاسفانه توانمندی‌های شما مطابق توانمندی‌های موردنیاز نبود.',
}

export const feedback1 = {
  charityName: 'موسسه سما',
  rating: 4,
  comment: 'بسیار عالی و وقت‌شناس. تمام جلسات وعده داده حضور پیدا کردند.',
}

export const feedback2 = {
  charityName: 'موسسه حکمت',
  rating: 2,
}

export const feedback3 = {
  charityName: 'پناهگاه وفا',
  rating: 5,
  comment: 'عالی عالی عالی!!',
}

export const feedback4 = {
  charityName: 'موسسه امیر',
  rating: 3,
  comment: 'خدمات متوسط اما شخصیت عالی',
}