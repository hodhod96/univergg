export const messages = {
  APP_DESCRIPTION: 'سامانه ارائه خدمات مشاوره تحصیلی و کاری',
  WELCOME_MESSAGE: 'به یونیورجی‌جی خوش آمدید.',
  SUBMIT: 'ثبت',
  USERNAME: 'نام کاربری',
  PASSWORD: 'رمز عبور',
  NAME: 'نام و نام خانوادگی',
  EMAIL: 'ایمیل',
  CONFIRM_PASS: 'تکرار رمز عبور',
  GENDER_T: 'جنسیت',
  AGE_T: 'سن',
  PHONE_NUMBER_T: 'شماره تماس',
  CITY_T: 'شهر',
  COUNTRY_T: 'کشور',
  EDUCATION: 'سطح تحصیلات',
  STUDENT_LEVEL: 'آخرین مقطع تحصیلی',
  RECORD: 'سابقه',
  LOGIN: 'ورود',
  NOT_A_MEMBER: 'عضو یونیورجی‌جی نیستم!',
  SIGN_UP: 'عضویت',
  ADVISER: 'مشاور',
  STUDENT: 'دانشجو',

  RULES: 'قوانین و مقررات ',
  AGREEMENT_STATEMENT: 'را مطالعه کرده و قبول دارم.',
  PERSONAL_INFO: 'مشخصات فردی',
  INFO: 'مشخصات',

  EDIT: 'ویرایش',

  OUTGOING_REQUESTS: 'درخواست‌های من',
  STUDENT_MESSAGE: 'پیام دانشجو',

  YOUR_REQUEST_FOR_ADVISER: 'درخواست شما برای مشاوره به ',
  HAS_BEEN_SENT: ' فرستاده شد.',
  ADVISER_REASON: 'دلایل مشاور',
  REJECTED: 'رد شده',
  REJECTED_BY: 'رد کرده',
  ACCEPTED_BY: 'قبول کرده',
  PENDING: 'در دست بررسی',
  ACCEPTED: 'قبول شده',

  SHOW: 'مشاهده',

  VOLUNTEER_AGE: 'محدوده سنی مورد نیاز: ',
  VOLUNTEER_GENDER: 'جنسیت متقاضیان: ',
  PROJECT_LOCATION: 'موقعیت مکانی: ',

  SEND_REQUEST: 'ارسال درخواست',

  SEND_REQUEST_TO_VOLUNTEER: 'درخواست همکاری',

  CANCEL: 'انصراف',
  SEND: 'ارسال',
  VERIFY: 'تایید',
  OK: 'باشه',
  CONTINUE: 'ادامه',

  SEARCH: 'جستجو',
  ADD_FILTER: 'اضافه کردن فیلتر',

  FULL_NAME: 'نام و نام خانوادگی: ',
  CITY: 'شهر سکونت: ',
  PHONE_NUMBER: 'شماره تماس: ',
  GENDER: 'جنسیت: ',
  AGE: 'سن: ',

  ADDRESS: 'آدرس: ',
  ADDRESS_T: 'آدرس',
  OTHER_INFO: 'سوابق و اطلاعات دیگر',


  WOMAN: 'زن',
  MAN: 'مرد',

  LOGOUT_MESSAGE: 'آیا می‌خواهید از حساب خود خارج شوید؟',

  REQUEST_MESSAGE: 'در صورتی که توضیحی دارید، در فیلد پایین وارد کنید.',
  REJECT_REQUEST: 'رد درخواست',
  REJECT_REASON: 'در صورت تمایل، دلیل خود را برای رد درخواست وارد کنید.',

  ELEMENTARY_SCHOOL: 'متوسطه اول',
  PRE_HIGH_SCHOOL: 'متوسطه دوم',
  HIGH_SCHOOL: 'دبیرستان',

  DIPLOMA: 'دیپلم',
  BACHELOR: 'لیسانس',
  MASTERS: 'فوق‌لیسانس',
  PHD: 'دکتری',

  DETAILED_ERROR: '- فیلد سن نمی‌تواند خالی باشد.\n - شماره تلفن باید با 98+ شروع شود و درست باشد.'
}