import {createStackNavigator} from 'react-navigation'
import Launch from 'src/components/init/Launch'
import AuthNavigator from 'src/navigators/AuthNavigator'
import MainTabNavigator from 'src/navigators/MainTabNavigator'
import SearchPage from 'src/components/home/search/SearchPage'
import AdviserPage from 'src/components/home/AdviserPage'
import FilterPage from 'src/components/home/search/FilterPage'
import StudentEditProfile from 'src/components/profile/StudentEditProfile'
import AdviserEditProfile from 'src/components/profile/AdviserEditProfile'


const MainNavigator = createStackNavigator(
  {
    Launch: {screen: Launch},
    AuthNavigator: {screen: AuthNavigator},
    MainTabNavigator: {screen: MainTabNavigator},
    AdviserPage: {screen: AdviserPage},
    SearchPage: {screen: SearchPage},
    FilterPage: {screen: FilterPage},
    StudentEditProfile: {screen: StudentEditProfile},
    AdviserEditProfile: {screen: AdviserEditProfile},
  },
  {
    initialRouteName: 'Launch',
    headerMode: 'none',
    cardStyle: {backgroundColor: '#F3F5F7'},
    navigationOptions: {
      gesturesEnabled: false,
    },
  },
)


export default MainNavigator
