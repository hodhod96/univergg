export const Constants = {
  localServerUrl: 'http://192.168.203.169:8000/',
  serverUrl: 'http://194.5.192.18:8000/',

  loginUrl: '/login/',
  studentJoinUrl: '/student-register/',
  profileUrl: '/user/',
  adviserJoinUrl: '/advisor-register/',
  getAdvisersUrl: '/advisors-list/',
  requestUrl: '/request-consultation/',
  outgoingRequestsUrl: '/sent-requests/',
  incomingRequestsUrl: '/received-requests/',
  requestResponseUrl: '/send-request-response/',
  studentEditUrl: '/student-edit-profile/',
  adviserEditUrl: '/advisor-edit-profile/',

}